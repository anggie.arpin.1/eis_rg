<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    function index()
    {
        return view('pages.home');
    }

    function researcher()
    {
        return view('pages.researcher');
    }

    function theme()
    {
        return view('pages.theme');
    }

    function researchandproject()
    {
        return view('pages.project');
    }

    function gallery()
    {
        return view('pages.gallery');
    }

    function agenda()
    {
        return view('pages.agenda');
    }

    function publication()
    {
        return view('pages.publication');
    }

    function contact()
    {
        return view('pages.contact');
    }

    function details()
    {
        return view('pages.detailsAgenda');
    }
}
