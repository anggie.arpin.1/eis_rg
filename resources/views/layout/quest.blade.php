<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>{{ config('app.name') }} | @yield('header')</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('favicon.ico') }}" rel="icon">
    <link href="{{ asset('assets/quest/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/quest/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/quest/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/quest/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/quest/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/quest/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/quest/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/quest/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/quest/css/style.css') }}" rel="stylesheet">


</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center">
        <div class="container d-flex align-items-center">

            <a href="{{ route('home') }}" class="logo me-auto"><img src="{{ asset('assets/quest/img/eiscopy.png') }}"
                    alt="" class="img-fluid"></a>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a href="{{ route('home') }}"
                            class="{{ request()->routeIs('home') ? 'active' : '' }}">Home</a>
                    </li>

                    <li class="dropdown"><a href="#"
                            class="{{ request()->routeIs('researcher') || request()->routeIs('theme') || request()->routeIs('researchandproject') || request()->routeIs('gallery') ? 'active' : '' }}"><span>Research</span>
                            <i class="bi bi-chevron-down"></i></a>
                        <ul>
                            <li><a href="{{ route('researcher') }}"
                                    class="{{ request()->routeIs('researcher') ? 'active' : '' }}">Researcher</a></li>
                            <li><a href="{{ route('theme') }}"
                                    class="{{ request()->routeIs('theme') ? 'active' : '' }}">Research Theme and
                                    Topics</a></li>
                            <li><a href="{{ route('researchandproject') }}"
                                    class="{{ request()->routeIs('researchandproject') ? 'active' : '' }}">Research and
                                    Project</a>
                            </li>
                            <li><a href="{{ route('gallery') }}"
                                    class="{{ request()->routeIs('gallery') ? 'active' : '' }}">Research Gallery</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="{{ route('agenda') }}"
                            class="{{ request()->routeIs('agenda') || request()->routeIs('details') ? 'active' : '' }}">Agenda</a>
                    </li>
                    <li><a href="{{ route('publication') }}"
                            class="{{ request()->routeIs('publication') ? 'active' : '' }}">Publication</a></li>
                    <li><a href="{{ route('contact') }}"
                            class="{{ request()->routeIs('contact') ? 'active' : '' }}">Contact</a></li>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->

        </div>
    </header><!-- End Header -->

    @yield('hero')

    <main id="main">
        @yield('content')
    </main><!-- End #main -->

    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-5 col-md-5 col-12">
                        <div class="footer-info">
                            <a href="" class="logo me-auto"><img src="{{ asset('assets/quest/img/eis.png') }}"
                                    alt="" class="img-fluid img-footer mb-4"></a>
                            <p>
                                Kampus Tengah Undiksha,
                                Udayana No. 11, <br>
                                Singaraja, Bali, Indonesia <br>
                                81116<br><br>
                                <strong>Phone:</strong> +62362-22571<br>
                                <strong>Email:</strong> rasben.dantes@undiksha.ac.id <br>
                            </p>
                            <div class="social-links mt-3">
                                <a href="https://www.instagram.com/undiksha.bali/" class="instagram" target="blank"><i
                                        class="bx bxl-instagram"></i></a>
                                <a href="https://www.facebook.com/undiksha.bali" class="facebook" target="blank"><i
                                        class="bx bxl-facebook"></i></a>
                                <a href="https://twitter.com/i/flow/login?redirect_after_login=%2Fundikshabali"
                                    class="twitter" target="blank"><i class="bx bxl-twitter"></i></a>
                                <a href="https://www.youtube.com/universitaspendidikanganesha" class="youtube"
                                    target="blank"><i class="bx bxl-youtube"></i></a>
                                <a href="https://www.tiktok.com/@undiksha.bali" class="tiktok" target="blank"><i
                                        class="bx bxl-tiktok"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-6 footer-links">
                        <h4>Research</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('researcher') }}">Researcher</a>
                            </li>
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('theme') }}">Research Theme and
                                    Topics</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a
                                    href="{{ route('researchandproject') }}">Research Theme and
                                    Project</a>
                            </li>
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('gallery') }}">Research
                                    Gallery</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-3 col-6 footer-links">
                        <h4>More</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('agenda') }}">Agenda</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a
                                    href="{{ route('publication') }}">Publication</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="{{ route('contact') }}">Contact</a></li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>

        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong><span>Entreprise Information System - Research Group</span></strong>.
            </div>
            <div class="credits">
                <a href="https://undiksha.ac.id/" target="blank">Universitas Pendidikan Ganesha</a>
            </div>
        </div>
    </footer>


    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/quest/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/quest/vendor/glightbox/js/glightbox.min.js') }}"></script>
    <script src="{{ asset('assets/quest/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/quest/vendor/swiper/swiper-bundle.min.js') }}"></script>
    <script src="{{ asset('assets/quest/vendor/waypoints/noframework.waypoints.js') }}"></script>
    <script src="{{ asset('assets/quest/vendor/php-email-form/validate.js') }}"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('assets/quest/js/main.js') }}"></script>

</body>

</html>
