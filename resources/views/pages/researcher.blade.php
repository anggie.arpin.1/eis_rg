@extends('layout.quest')

@section('header')
    Researcher
@endsection

@section('hero')
@endsection

@section('content')
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Researcher</h2>
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Researcher</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <section id="team" class="team ">
        <div class="container">
            <h4>Lecturer</h4>
            <div class="row">
                <div class="col-lg-6">
                    <div class="member d-flex align-items-start">
                        <div class="pic"><img src="assets/quest/img/researcher/rasben.jpg" class="img-fluid"
                                alt="">
                        </div>
                        <div class="member-info">
                            <h4>Prof. Dr. Gede Rasben Dantes, S.T., M.T.I</h4>
                            <span>Head of Research Group</span>
                            <p>Information System</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="member d-flex align-items-start">
                        <div class="pic"><img src="assets/quest/img/researcher/ardwi.jpg" class="img-fluid"
                                alt="">
                        </div>
                        <div class="member-info">
                            <h4>I Made Ardwi Pradnyana. S.T., M.T.</h4>
                            <p>Information System</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="member d-flex align-items-start">
                        <div class="pic"><img src="assets/quest/img/researcher/lanang.jpg" class="img-fluid"
                                alt="">
                        </div>
                        <div class="member-info">
                            <h4>I Gusti Lanang Agung Raditya Putra, S.Pd.,M.T.</h4>
                            <p>Information System</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr>

            <h4>Master Degree</h4>
            <div class="row">
                <p class="text-center">Coming Soon</p>
            </div>
            <hr>

            <h4>Bachelor Degree</h4>
            <div class="row">
                <p class="text-center">Coming Soon</p>
            </div>
        </div>
    </section><!-- End Team Section -->
@endsection
