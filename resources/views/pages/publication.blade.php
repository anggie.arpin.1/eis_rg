@extends('layout.quest')

@section('header')
    Publication
@endsection

@section('hero')
@endsection

@section('content')
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Publication</h2>
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Publication</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <section id="services" class="services">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama/Judul</th>
                                            <th scope="col">Tahun Publikasi</th>
                                            <th scope="col">Publisher</th>
                                            <th scope="col">Penulis</th>
                                            <th scope="col">Link Publikasi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Usability testing on website wadaya based on ISO 9241-11</td>
                                            <td>2019</td>
                                            <td>Journal of Physics</td>
                                            <td>I K R Arthana, I M A Pradnyana, and G R Dantes</td>
                                            <td><a href="https://iopscience.iop.org/article/10.1088/1742-6596/1165/1/012012/pdf"
                                                    class="text-decoration-none"
                                                    target="blank">https://iopscience.iop.org/article/10.1088/1742-6596/1165/1/012012/pdf</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
