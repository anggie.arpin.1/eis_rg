@extends('layout.quest')

@section('header')
    Home
@endsection

@section('hero')
    <!-- ======= Hero Section ======= -->
    <section id="hero">
        <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">

            <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

            <div class="carousel-inner" role="listbox">

                <!-- Slide 1 -->
                <div class="carousel-item active" style="background-image: url(assets/quest/img/slide/slide-1.jpg)">
                    <div class="carousel-container">
                        <div class="container">
                            <h2 class="animate__animated animate__fadeInDown">Welcome to <span>EIS-RG</span></h2>
                            <p class="animate__animated animate__fadeInUp">Integrated Information Technology solutions
                                for
                                business processes and activities.</p>
                        </div>
                    </div>
                </div>

                <!-- Slide 2 -->
                <div class="carousel-item" style="background-image: url(assets/quest/img/slide/slide-2.jpg)">
                    <div class="carousel-container">
                        <div class="container">
                            <h2 class="animate__animated animate__fadeInDown">Entreprise Information System Research Group
                            </h2>
                            <p class="animate__animated animate__fadeInUp">Optimization of the role of Information
                                Technology in supporting business/
                                organizations starts from strategic, managerial
                                and operational aspects.</p>
                        </div>
                    </div>
                </div>

            </div>

            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
                <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
            </a>

            <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
                <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
            </a>

        </div>
    </section><!-- End Hero -->
@endsection

@section('content')
    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
        <div class="container" data-aos="fade-up">

            <div class="row">

                <div class="col-lg-8 entries">
                    <h3 class="mb-3">Recent Information</h3>
                    <div class="row">
                        <div class="col-md-4">
                            <a href="{{ route('researchandproject') }}" class="blog-home">
                                <img src="{{ asset('assets/quest/img/gallery/rudaya.png') }}" alt=""
                                    class="img-fluid">
                                <div class="description mt-3">
                                    <h6>Rudaya ~ Connect The Art</h6>
                                    <p class="date"><i class="bi bi-clock"></i> 1 September 2023</p>
                                    <p>Rudaya adalah sebuah media aplikasi berbasis mobile yang menjembatani para pencari
                                        seni dengan para seniman kesenian dan kebudayaan daerah di seluruh Indonesia</p>
                                </div>
                                <hr>
                            </a>
                        </div>
                    </div>

                    <div class="blog-pagination mb-3">
                        <ul class="justify-content-center">
                            <li class="active"><a href="{{ route('researchandproject') }}">See More</a></li>
                        </ul>
                    </div>

                </div><!-- End blog entries list -->

                <div class="col-lg-4">

                    <div class="sidebar">
                        <h3 class="sidebar-title">Agenda</h3>
                        <div class="sidebar-item recent-posts">
                            <div class="post-item clearfix">
                                <img src="{{ asset('assets/quest/img/icon/calendar.jpg') }}" alt="">
                                <h4><a href="">Launching Entreprise Information System - Research Group</a></h4>
                                <time datetime="2020-01-01">1 September 2023</time>
                            </div>
                            <hr>
                        </div><!-- End sidebar recent posts-->

                        <h3 class="sidebar-title">Visit too</h3>
                        <div class="sidebar-item tags">
                            <a href="https://undiksha.ac.id/" class="btn btn-outline-secondary d-block"> <i
                                    class='bx bx-world me-2'></i> Undiksha Website</a>
                        </div><!-- End sidebar tags-->

                    </div><!-- End sidebar -->

                </div><!-- End blog sidebar -->

            </div>

        </div>
    </section><!-- End Blog Section -->
@endsection
