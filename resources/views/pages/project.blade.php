@extends('layout.quest')

@section('header')
    Research & Project
@endsection

@section('hero')
@endsection

@section('content')
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Research and Project</h2>
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Research and Project</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <section id="blog" class="blog">
        <div class="container" data-aos="fade-up">

            <div class="row">

                <div class="col-lg-8 entries">
                    <article class="entry">

                        <div class="entry-img">
                            <img src="{{ asset('assets/quest/img/gallery/rudaya.png') }}" alt="" class="img-fluid">
                        </div>

                        <h2 class="entry-title">
                            <a href="">Rudaya ~ Connect The Art</a>
                        </h2>

                        <div class="entry-meta">
                            <ul>
                                <li class="d-flex align-items-center"><i class="bi bi-person"></i> <a href="">I Gusti
                                        Lanang Agung Raditya Putra,
                                        S.Pd.,M.T. dkk.</a></li>
                                <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href=""><time
                                            datetime="2020-01-01">Jan 1,
                                            2020</time></a></li>
                                <li class="d-flex align-items-center"><i class="bi bi-arrow-right-square-fill"></i><a
                                        href="">Project
                                        based</a></li>
                            </ul>
                        </div>
                        <div class="entry-content">
                            <p>
                                Rudaya adalah sebuah media aplikasi berbasis mobile yang menjembatani para pencari seni
                                dengan para seniman kesenian dan kebudayaan daerah di seluruh Indonesia
                            </p>
                            <div class="read-more">
                                <a href="https://play.google.com/store/apps/details?id=id.rudaya.rudayaapp"
                                    target="blank">See More</a>
                            </div>
                        </div>

                    </article><!-- End blog entry -->

                    <div class="blog-pagination">
                        <ul class="justify-content-center">
                            <li class="active"><a href="#">1</a></li>
                            <li class=""><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                        </ul>
                    </div>

                </div><!-- End blog entries list -->

                <div class="col-lg-4">

                    <div class="sidebar">

                        <h3 class="sidebar-title">Search</h3>
                        <div class="sidebar-item search-form">
                            <form action="">
                                <input type="text">
                                <button type="submit"><i class="bi bi-search"></i></button>
                            </form>
                        </div><!-- End sidebar search formn-->

                        <h3 class="sidebar-title">Theme</h3>
                        <div class="sidebar-item categories">
                            <ul>
                                <li><a href="#">Entreprise Resource Planning <span>(0)</span></a></li>
                                <li><a href="#">Business Process Management <span>(0)</span></a></li>
                                <li><a href="#">IT Stategic Planning <span>(0)</span></a></li>
                                <li><a href="#">IT Governance <span>(0)</span></a></li>
                                <li><a href="#">IT Service Management <span>(0)</span></a></li>
                                <li><a href="#">Information System Analysis and Design <span>(0)</span></a></li>
                                <li><a href="#">E-Government <span>(0)</span></a></li>
                                <li><a href="#">User Experience <span>(0)</span></a></li>
                            </ul>
                        </div><!-- End sidebar categories-->

                        <h3 class="sidebar-title">Recent Research and Project</h3>
                        <div class="sidebar-item recent-posts">
                            <div class="post-item clearfix">
                                <img src="{{ asset('assets/quest/img/gallery/rudaya.png') }}" alt="">
                                <h4><a href="">Rudaya ~ Connect The Art</a></h4>
                                <time datetime="2020-01-01">Jan 1, 2020</time>
                            </div>
                        </div><!-- End sidebar recent posts-->

                    </div><!-- End sidebar -->

                </div><!-- End blog sidebar -->

            </div>

        </div>
    </section>
@endsection
