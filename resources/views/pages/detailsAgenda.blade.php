@extends('layout.quest')

@section('header')
    Details Agenda
@endsection

@section('hero')
@endsection

@section('content')
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Agenda</h2>
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Agenda</li>
                    <li>Details</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <section id="blog" class="blog">
        <div class="container" data-aos="fade-up">

            <div class="row">

                <div class="col-lg-8 entries">

                    <article class="entry entry-single">
                        <h2 class="entry-title">
                            <a href="">Launching Entreprise Information System - Research Group</a>
                        </h2>

                        <div class="entry-meta">
                            <ul>
                                <li class="d-flex align-items-center"><i class="bi bi-clock"></i> <a href=""><time
                                            datetime="2020-01-01">1 September 2023</time></a></li>
                                <li class="d-flex align-items-center text-success"><i class="bi bi-megaphone"></i>Online
                                </li>
                            </ul>
                        </div>

                        <div class="entry-content">
                            <p>
                                Kami mengumumkan peluncuran resmi Entreprise Information System Research Group, sebuah
                                langkah maju dalam mendorong pemahaman dan pengembangan sistem informasi perusahaan yang
                                efisien.
                            </p>
                            <a href="{{ url()->previous() }}" class="btn btn-outline-secondary">Kembali</a>
                        </div>
                    </article><!-- End blog entry -->

                </div><!-- End blog entries list -->

                <div class="col-lg-4">

                    <div class="sidebar">
                        <h3 class="sidebar-title">Other Agenda</h3>
                        <div class="sidebar-item recent-posts">
                            <div class="post-item clearfix">
                                <img src="{{ asset('assets/quest/img/icon/calendar.jpg') }}" alt="">
                                <h4><a href="">Launching Entreprise Information System - Research Group</a></h4>
                                <time datetime="2020-01-01">1 September 2023</time>
                            </div>
                            <hr>
                        </div><!-- End sidebar recent posts-->


                    </div><!-- End sidebar -->

                </div><!-- End blog sidebar -->

            </div>

        </div>
    </section>
@endsection
