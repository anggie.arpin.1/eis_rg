@extends('layout.quest')

@section('header')
    Agenda
@endsection

@section('hero')
@endsection

@section('content')
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Agenda</h2>
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Agenda</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <section id="services" class="services">
        <div class="container">

            <div class="row">
                <div class="col-md-12 col-12">
                    <div class="icon-box">
                        <div class="row">
                            <div class="col-md-9">
                                <i class="bi bi-info-circle"></i>
                                <h4><a href="#">Launching Entreprise Information System - Research Group</a></h4>
                                <p><small>1 September 2023 - <span class="text-success">Online</span></small></p>
                                <p>Kami mengumumkan peluncuran resmi Entreprise Information System Research Group, sebuah
                                    langkah
                                    maju dalam mendorong pemahaman dan pengembangan sistem informasi perusahaan yang
                                    efisien.</p>
                            </div>
                            <div class="col-md-3">
                                <p class="text-end"><a href="{{ route('details') }}" class="btn btn-outline-secondary">Lihat
                                        Selengkapnya</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
