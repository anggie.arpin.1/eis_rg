@extends('layout.quest')

@section('header')
    Contact
@endsection

@section('hero')
@endsection

@section('content')
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Contact</h2>
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Contact</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="mb-4">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3949.8398855720193!2d115.08536827489846!3d-8.117779891911589!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd19a0cfc4df37f%3A0x21b1b90655c9af1b!2sFakultas%20Teknik%20dan%20Kejuruan%2C%20Universitas%20Pedidikan%20Ganesha!5e0!3m2!1sid!2sid!4v1692889045528!5m2!1sid!2sid"
                        style="border:0; width: 100%; height: 270px; border:0;" allowfullscreen="" loading="lazy"
                        referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div class="col-lg-6 mt-5 mt-lg-0">
                    <div class="head">
                        <h4>Prof. Dr. Gede Rasben Dantes, S.T., M.T.I.</h4>
                        <h6>Head of Research Group</h6>
                    </div>
                    <div class="info mt-5 mb-4">
                        <p></p>
                        <div class="address">
                            <i class="bi bi-geo-alt"></i>
                            <h4>Secretariat:</h4>
                            <p>Faculty of Engineering and
                                Vocational, Universitas Pendidikan Ganesha</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="info">
                        <div class="address">
                            <i class="bi bi-geo-alt"></i>
                            <h4>Location:</h4>
                            <p>Kampus Tengah Undiksha, Jalan
                                Udayana No. 11, Singaraja, Bali, Indonesia -
                                81116</p>
                        </div>

                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="email">
                                    <i class="bi bi-envelope"></i>
                                    <h4>Email:</h4>
                                    <p> rasben.dantes@undiksha.ac.id.</p>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="phone">
                                    <i class="bi bi-phone"></i>
                                    <h4>Call:</h4>
                                    <p>+62362-22571</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>

        </div>
    </section>
@endsection
