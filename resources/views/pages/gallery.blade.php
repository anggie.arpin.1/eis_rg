@extends('layout.quest')

@section('header')
    Gallery
@endsection

@section('hero')
@endsection

@section('content')
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Research Gallery</h2>
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Research Gallery</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <section id="portfolio" class="portfolio">
        <div class="container">

            <div class="row">
                <div class="col-lg-12 d-flex justify-content-center">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">All</li>
                        <li data-filter=".ERP">ERP</li>
                        <li data-filter=".ITSP">IT Strategic Planning</li>
                        <li data-filter=".BPM">BPM</li>
                        <li data-filter=".ITG">IT Governance</li>
                        <li data-filter=".ITSM">IT Service ManagementM</li>
                        <li data-filter=".Egov">E-Government</li>
                        <li data-filter=".ISAD">IS Analysis and Design</li>
                        <li data-filter=".UX">User Experience</li>
                    </ul>
                </div>
            </div>

            <div class="row portfolio-container">

                <div class="col-lg-4 col-md-6 portfolio-item UX">
                    <div class="portfolio-wrap">
                        <img src="{{ asset('assets/quest/img/gallery/rudaya.png') }}" class="img-fluid" alt="">
                        <div class="portfolio-info">
                            <h4>Rudaya Connect The Art</h4>
                            <p class="author-gallery">I Gusti Lanang Agung Raditya Putra, S.Pd.,M.T.</p>
                            <div class="portfolio-links">
                                <a href="portfolio-details.html" class="gallery-link" data-glightbox="type: external"
                                    title="Portfolio Details">See More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
