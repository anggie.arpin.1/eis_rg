@extends('layout.quest')

@section('header')
    Theme & Topics
@endsection

@section('hero')
@endsection

@section('content')
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container mt-2">
            <div class="d-flex justify-content-between align-items-center">
                <h2>Research Theme and Topics</h2>
                <ol>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li>Research Theme and Topics</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <section id="services" class="services">
        <div class="container">

            <div class="row">
                <div class="col-md-6 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="bi bi-rocket-takeoff"></i>
                        <h4><a href="#">StartUp</a></h4>
                        <p>A dynamic venture, a startup pioneers innovation, disrupts markets, and envisions growth in a
                            rapidly evolving business landscape.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="icon-box">
                        <i class="bi bi-diagram-3"></i>
                        <h4><a href="#">Enterprise Resource Planning</a></h4>
                        <p>ERP (Enterprise Resource Planning) is an integrated software system that facilitates seamless
                            management of various business processes and resources within an organization.</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="bi bi-card-checklist"></i>
                        <h4><a href="#">IT Strategic Planning</a></h4>
                        <p>IT Strategic Planning is the process of defining and aligning an organization's technological
                            goals and resources to achieve its overall business objectives.</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="bi bi-bar-chart-steps"></i>
                        <h4><a href="#">Business Process Management</a></h4>
                        <p>Business Process Management (BPM) is a systematic approach to optimize and streamline
                            organizational processes to enhance efficiency, effectiveness, and adaptability</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="bi bi-hdd-rack"></i>
                        <h4><a href="#">IT Governance</a></h4>
                        <p>"IT Governance is the framework of processes, structures, and controls ensuring effective and
                            responsible management of information technology in organizations."</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="bi bi-people-fill"></i>
                        <h4><a href="#">IT Service Management</a></h4>
                        <p>IT Service Management (ITSM) is the practice of designing, delivering, managing, and improving IT
                            services to meet business needs effectively.</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="bi bi-bank"></i>
                        <h4><a href="#">E-Government</a></h4>
                        <p>E-Government is the digital transformation of government services and processes for efficient
                            public interaction and administration.</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="bi bi-bounding-box-circles"></i>
                        <h4><a href="#">Information Systems Analysis and Design</a></h4>
                        <p>Information System Analysis and Design is a process of analyzing, planning, and creating
                            efficient and effective information systems.</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="bi bi-phone"></i>
                        <h4><a href="#">User Experience: website and application
                                usability testing (desktop and mobile)</a></h4>
                        <p>"User Experience (UX) is the overall feeling and satisfaction a user gets when interacting with a
                            product or service."</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="bi bi-briefcase"></i>
                        <h4><a href="#">E-Business</a></h4>
                        <p>E-business refers to conducting commercial activities online, including buying, selling,
                            marketing, and managing operations through digital platforms.</p>
                    </div>
                </div>
                <div class="col-md-6 mt-4 mt-md-0">
                    <div class="icon-box">
                        <i class="bi bi-bag-check"></i>
                        <h4><a href="#">E-Commerce</a></h4>
                        <p>E-commerce: Online platform enabling buying and selling of products/services, revolutionizing
                            traditional commerce with digital transactions and global reach.</p>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
