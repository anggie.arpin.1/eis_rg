<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/researcher', [HomeController::class, 'researcher'])->name('researcher');
Route::get('/themeandtopics', [HomeController::class, 'theme'])->name('theme');
Route::get('/researchandproject', [HomeController::class, 'researchandproject'])->name('researchandproject');
Route::get('/gallery', [HomeController::class, 'gallery'])->name('gallery');
Route::get('/agenda', [HomeController::class, 'agenda'])->name('agenda');
Route::get('/agenda/details', [HomeController::class, 'details'])->name('details');
Route::get('/publication', [HomeController::class, 'publication'])->name('publication');
Route::get('/contact', [HomeController::class, 'contact'])->name('contact');
